#!/bin/bash
git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/InstitutMaupertuis/seekware_sdk.git
cd seekware_sdk
./install_linux.bash x86_64-linux-gnu skip
cd -
rm -rf seekware_sdk
