cmake_minimum_required(VERSION 3.2)
project(seek_thermal)
add_compile_options(-Wall -Wextra)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  roscpp
  sensor_msgs
  std_msgs
)

list(
  APPEND
  CMAKE_MODULE_PATH
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules/
)
find_package(SeekWare REQUIRED)

set(
  ${PROJECT_NAME}_ROSMASTER_HOSTNAME
  "localhost"
  CACHE
  STRING
  "ROS master hostname"
)

set(
  ${PROJECT_NAME}_TOPIC
  "/thermal_camera/seek/mosaic_core_S304SP/raw"
  CACHE
  STRING
  "Raw image topic"
)

################################################
## Declare ROS messages, services and actions ##
################################################

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

###################################
## catkin specific configuration ##
###################################

catkin_package(
  CATKIN_DEPENDS
  cv_bridge
  roscpp
  sensor_msgs
  std_msgs
  DEPENDS
  SeekWare
)

###########
## Build ##
###########

include_directories(
  ${SeekWare_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

add_executable(
  ${PROJECT_NAME}_raw_publisher
  src/raw_publisher.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_raw_publisher
  ${SeekWare_LIBRARIES}
  ${catkin_LIBRARIES}
)
set_target_properties(
  ${PROJECT_NAME}_raw_publisher
  PROPERTIES
  OUTPUT_NAME
  "raw_publisher"
)

#############
## Install ##
#############

## Mark executables and/or libraries for installation
install(
  TARGETS
  ${PROJECT_NAME}_raw_publisher
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

# Configure run bash script
configure_file(
  scripts/run.bash.in
  scripts/run.bash
)

# Configure service script
# The script is NOT installed because it would require sudo rights
# More info in the README
configure_file(
  scripts/ros_${PROJECT_NAME}.service.in
  scripts/ros_${PROJECT_NAME}.service
)

# Install bash scripts
install(
  PROGRAMS
  ${CMAKE_CURRENT_BINARY_DIR}/scripts/run.bash
  DESTINATION
  ${CATKIN_PACKAGE_BIN_DESTINATION}
  PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
)

#############
## Testing ##
#############
