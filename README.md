[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Dependencies
- [SeekWare SDK](https://developer.thermal.com/)

# Output images
- Output images are grayscale 16 bit images.
- Seek Thermal pixels are coded on 16 bits (0 to 65 535).

In RViz tick the `Normalize range` option to easily view a thermal image.

# Usage
Run the node:
```bash
rosrun seek_thermal raw_publisher
```

## CMake variables
- `seek_thermal_ROSMATER_HOSTNAME`: Specify to which ROS master the node will connect
- `seek_thermal_TOPIC`: Specify the topic on which images will be published

# Automatic startup
This setup allows to automatically start the `raw_publisher` node at startup.

On startup the node waits for ROS master to be available.
The node will shut down when the ROS master is not available, `systemd` will the restart the node.

This way the node is always running / publishing when the ROS master is available.

## systemd
```bash
sudo cp build/seek_thermal/scripts/ros_seek_thermal.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable ros_seek_thermal
sudo systemctl start ros_seek_thermal
```
