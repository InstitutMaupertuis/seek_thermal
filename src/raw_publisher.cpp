#include <cv_bridge/cv_bridge.h>
#include <ros/ros.h>
#include <seekware/seekware.h>

void print_fw_info(psw camera)
{
  sw_retcode status;
  int therm_ver;

  ROS_INFO_STREAM("Model Number: " << camera->modelNumber);
  ROS_INFO_STREAM("SerialNumber: " << camera->serialNumber);
  ROS_INFO_STREAM("Manufacture Date: "<<  camera->manufactureDate);
  ROS_INFO_STREAM("Firmware Version: " << (unsigned) camera->fw_version_major << "." << (unsigned) camera->fw_version_minor
                                    << (unsigned) camera->fw_build_major << "." << (unsigned) camera->fw_build_minor);

  status = Seekware_GetSettingEx(camera, SETTING_THERMOGRAPHY_VERSION, &therm_ver, sizeof(therm_ver));
  if (status != SW_RETCODE_NONE)
    throw std::runtime_error("Error: Seek GetSetting returned " + std::to_string(status));
  ROS_INFO_STREAM("Themography Version: " << therm_ver);

  sw_sdk_info sdk_info;
  Seekware_GetSdkInfo(nullptr, &sdk_info);
  ROS_INFO_STREAM("Image Processing Version: " << (unsigned) sdk_info.lib_version_major << "." << (unsigned) sdk_info.lib_version_minor
            << "." << (unsigned) sdk_info.lib_build_major << "." << (unsigned) sdk_info.lib_build_minor);
}

void captureImages(ros::NodeHandle &nh,
                   psw camera,
                   ros::Publisher &pub)
{
  size_t camera_pixels ((size_t)camera->frame_cols * (size_t)camera->frame_rows);
  uint16_t *img_data(new uint16_t[camera_pixels + camera->frame_cols]);
  if (img_data == nullptr)
  {
    if (camera)
      Seekware_Close(camera);
    delete[] img_data;
    throw std::runtime_error("Cannot allocate filtered buffer!");
  }

  cv_bridge::CvImage raw;
  raw.encoding = sensor_msgs::image_encodings::MONO16;
  raw.header.frame_id = "seek_thermal";
  raw.header.seq = 0;
  raw.header.stamp = ros::Time::now();

  while (nh.ok() && ros::master::check() && ros::ok())
  {
    sw_retcode status(Seekware_GetImageEx(camera, img_data, nullptr, nullptr));
    if (status == SW_RETCODE_NOFRAME)
    {
      ROS_ERROR_STREAM("Seek Camera Timeout ...");
      continue;
    }
    if (status == SW_RETCODE_DISCONNECTED)
    {
      ROS_ERROR_STREAM("Seek Camera Disconnected ...");
      return;
    }
    if (status != SW_RETCODE_NONE)
    {
      ROS_ERROR_STREAM("Seek Camera Error: " << status);
      return;
    }

    raw.image = cv::Mat(camera->frame_rows, camera->frame_cols, CV_16UC1, img_data);
    raw.header.stamp = ros::Time::now();
    pub.publish(raw);
    ++raw.header.seq;
  }
}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "seek_thermal");
  ros::NodeHandle nh("~");

  sw_sdk_info sdk_info;
  Seekware_GetSdkInfo(nullptr, &sdk_info);
  ROS_INFO_STREAM("SDK Version: " << (unsigned) sdk_info.sdk_version_major << "." << (unsigned) sdk_info.sdk_version_minor);

  int num_cameras_found(0);
  psw camera_list[1]; // Expect only one camera
  sw_retcode status = Seekware_Find(camera_list, 1, &num_cameras_found);
  if (status != SW_RETCODE_NONE || num_cameras_found == 0)
  {
    ROS_ERROR_STREAM("Cannot find any cameras...exiting");
    return 1;
  }

  psw camera (camera_list[0]);
  status = Seekware_Open(camera);
  if (status != SW_RETCODE_NONE)
  {
    ROS_ERROR_STREAM("Cannot open camera: " << status);
    if (camera)
      Seekware_Close(camera);
    return 1;
  }

  // Must read firmware info AFTER the camera has been opened
  ROS_INFO_STREAM("Camera firmware info: ");
  print_fw_info(camera);

  uint32_t enable(1);
  Seekware_SetSettingEx(camera, SETTING_ENABLE_TIMESTAMP, &enable, sizeof(enable));
  Seekware_SetSettingEx(camera, SETTING_RESET_TIMESTAMP, &enable, sizeof(enable));

  std::string raw_topic("raw");
  nh.getParam("raw_topic", raw_topic);
  ros::Publisher pub = nh.advertise<sensor_msgs::Image>(raw_topic, 5);
  ros::AsyncSpinner s(0);
  s.start();

  ROS_INFO_STREAM("Starting to publish raw images");
  captureImages(nh, camera, pub);

  ROS_WARN_STREAM("ROS shutdown");
  if (camera)
    Seekware_Close(camera);
  return 0;
}
